#!/bin/sh

pkg_delete 'mq_hdfs-py*' 'py*-mq_hdfs-*'
rm -f MANIFEST
rm -rf build/ && python setup.py make_pkg --optimize=2 --extra-comment=" `svnversion`"

#python setup.py make_pkg --extra-comment=" `svnversion`"
