#!/usr/bin/env python
# -*- coding: utf8 -*-

# ultraviolance distutils hack
import re
import distutils.versionpredicate
distutils.versionpredicate.re_validPackage = re.compile(r"^\s*([a-z_-]+)(.*)", re.IGNORECASE)

from distutils.core import setup

try:
    import xpkg
except ImportError:
    pass


setup ( 
        name= 'mq_hdfs',
        version='0.3.2',
        #packages=packages,
        #package_dir = packages,
        author='Evgeny Turnaev',
        author_email='turnaev.e@gmail.com',
        description='Zmq http sink',
        long_description="""Pulls zmq json messages and performs http requests taking messages as fetch configuration.""",

        classifiers = ['devel'],
        data_files=[
                        ('libexec', ['src/mq_hdfs']),
                        ('etc', [
                                 'bsd_pkg/mq_json_click_hdfs.conf.sample',
                                 'bsd_pkg/mq_json_mail_auth_log.conf.sample',
                                 'bsd_pkg/mq_json_view_hdfs.conf.sample',
                                 'bsd_pkg/mq_json_test_click_hdfs.conf.sample',
                                 'bsd_pkg/mq_json_test_view_hdfs.conf.sample',
                                ]),
                   ],
        options={'build_pkg': {'name_prefix': True,
                               'python_min_version': 2.7,
                               'pkg_install': 'bsd_pkg/postinstall.sh',
                               'rc_scripts': ('bsd_pkg/mq_json_view_hdfs.in', 'bsd_pkg/mq_json_mail_auth_log', 'bsd_pkg/mq_json_click_hdfs.in',
                                              'bsd_pkg/mq_json_test_click_hdfs.in', 'bsd_pkg/mq_json_test_view_hdfs.in'),
                              }},
        requires = ['py-mqd (>=0.2.9)', 
                    'py-msgpack',
                    'py-cyhdfs (>=0.1)', 
                    'py-xutils (>=0.1.0)',
                   ],
        )



